// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

export default (req, res) => {
    const games = require('./data/games.json');

    if (!req.query.code || !req.query.guess || !games[req.query.code] || isNaN(parseInt(req.query.guess))) {
        res.statusCode = 400;
        return res.json({
            error: "true",
            errorReason: "Must provide valid game code and guess"
        })
    }

    const guess = parseInt(req.query.guess);
    const code = req.query.code;
    let response = '';
    if (guess < games[code].number) {
        response = 'TOO LOW'
    } else if (guess > games[code].number) {
        response = 'TOO HIGH'
    } else {
        response = 'CORRECT!'
    }

    res.statusCode = 200
    res.json({
        result: response
    })
}
