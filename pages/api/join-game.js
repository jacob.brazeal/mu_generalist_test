// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

export default (req, res) => {
    const games = require('./data/games.json');

    const codes = Object.keys(games);
    const code = codes[Math.random() * codes.length | 0];
    console.log(code);
    res.statusCode = 200
    res.json({
        code: code,
        low: games[code].low,
        high: games[code].high
    })
}
